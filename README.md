# location finder


# what is it?

find a location based on the zip code and the selected country

## how to use

use ``` npx serve -p 3000 ``` or run index.html on port 3000

# notes
the country list was generated from the csv file by the scan.py file into the countryList.json

the code was made from a video by Traversy Media
