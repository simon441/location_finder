import csv
import json
import collections

# aliases
OrderedDict = collections.OrderedDict

data = []
headers = []
headers = ['Country ', 'Code ', 'Example URL ', 'Range ', 'Count']
i = 0
src = 'countryList.tsv'
dest = 'countryList.json'
with open(src, 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')# , quotechar='"')
    for row in reader:
        if row[0].strip()[0] == '#':  #
            continue
        # row = filter(None, row)
        if i == 0:
            headers = row
            i += 1
        else:
            print(row)
            d = {}
            j = 0
            for line in row:
                d[headers[j].strip()] = line
                j += 1
            data.append(d)
            # data.append(OrderedDict(zip(headers, row)))

print(data)
with open(dest, 'w') as jsonfile:
    json.dump(data, jsonfile, indent=2)
